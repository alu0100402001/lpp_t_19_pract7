module QuestionExamen
  
  Node = Struct.new(:value, :next , :anterior) #define una estructura indicando los campos que contendrá
  
  class Pregunta
    
    attr_accessor :question
    
    def initialize(question)
      @question = question
      raise ArgumentError, 'Specify :question' unless @question
    end
    
  end #final de la clase pregunta
  
  ######################################################################################################
  
  
  class List
    
    attr_accessor :cabeza , :tail
     
    @cabeza = nil
    @tail = nil
    
     def push(nodo)
       if @cabeza == nil
	 @cabeza = nodo
	 @tail = nodo
       else #si cabeza apunta algun nodo
	nodo.next = @cabeza
	@cabeza.anterior = nodo
	@cabeza = nodo
	nodo.anterior = nil
	#@cabeza.anterior = nil
	#@cabeza.next = nodo.next
       end
    end
        
      def pop()
	  if (@cabeza != @tail)#si la lista tiene mas de un elemento
	    aux = @cabeza
	    @cabeza = @cabeza.next
	    @cabeza.anterior = nil
	    aux.next = nil
	    aux.value
	  else
	    @cabeza = nil
	    @tail = nil
	    
	  end
     end
     
     def push_final(nodo)
       if @tail == nil
	 @tail = nodo
	 @cabeza = nodo
       else
	  @tail.next = nodo
	  nodo.anterior = @tail
	  @tail = nodo
       end
     end
     
     def pop_final()
       if (@cabeza != @tail)#si la lista tiene mas de un elemento
	    aux = @tail
	    @tail = @tail.anterior
	    @tail.next = nil
	    aux.anterior = nil
	    aux.value
	  else
	    @cabeza = nil
	    @tail = nil
	  end
     end
end
  
  
  ######################################################################################################
  
  class SeleccionSimple < Pregunta
    
    attr_accessor :right, :distractor
    
    def initialize(question, right, distractor)
      super(question)
      
      @right = right
      raise ArgumentError, 'Specify :right' unless @right
      
      @distractor = distractor
      raise ArgumentError, 'Specify :distractor' unless @distractor
      
    end
    
    
    def to_html
      options = @distractor+[@right]
      options = options.shuffle # para mezclar el array para que no este la correcta siempre al final
      s = ' '
      options.each do |options|
	s += %Q{<input type = "radio" value= "#{options}" name = 0 > #{options}\n}
      end 
      "#{@text}<br/>\n#{s}\n" 
    end
    
   
    def to_s
      opcion = @distractor+[@right]
      s= ' '
      opcion.each do |opcion|
	
	s += %Q{#{opcion}\n}
	
      end
	 "#{@question}\n#{s}\n"
    end  
    
  end #fin de SeleccionSimple
  
  ######################################################################################################
  
  class VerdaderoFalso < Pregunta
    
    attr_accessor :right, :wrong
    
    def initialize(question, right, wrong)
      
      super(question)
      
      @right = right
      raise ArgumentError, 'Specify :right' unless @right
      
      @wrong = wrong
      raise ArgumentError, 'Specify :wrong' unless @wrong
      
    end
    
    
    def to_s
      opcion = @right+[@wrong]
      s= ' '
      opcion.each do |opcion|
	
	s += %Q{#{opcion}\n}
	
      end
	 "#{@question}\n#{s}\n"
    end  
    
  end #fin de VerdaderoFalso
  
  
end
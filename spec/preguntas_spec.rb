require "question_examen/pregunta"

module QuestionExamen
    
    
    describe QuestionExamen::Pregunta do	  
	  before :each do
	    @q = QuestionExamen::Pregunta.new('2+2 =')
	  end	  
	    
	  context "Pruebas para la clase Preguntas" do
	    it "Tiene que tener un campo para la pregunta" do
	      expect(@q.question) == '2+2 ='
	    end
	  end	    
    end#fin del describe de preguntas
    
    
    describe QuestionExamen::SeleccionSimple do	  
	  before :each do
	     @p1 = QuestionExamen::SeleccionSimple.new('Cual es el tipo del objeto en el siguiente codigo Ruby? class Objeto \n end', "c) Un Objeto \n",
 				 	["a) Una instancia de la clase Class \n ", "b) Una constante \n", "d) Ninguna de las anteriores \n"])
	     @node1 = Node.new(@p1,nil)	      
	  end	
	  
	  context "Pruebas para la clase Seleccion Simple" do
	    it "Es de la clase SeleccionSimple" do
               expect(@p1.class) ==  SeleccionSimple       
	    end
	    it "Debe tener un enunciado heredado de la clase Pregunta" do
               expect(@p1.question) == 'Cual es el tipo del objeto en el siguiente codigo Ruby? class Objeto \n end'	       
	    end
	    it "Debe tener una respuesta correcta" do
               expect(@p1.right) == 'c) Un Objeto \n'	       
	    end	
            it "Debe tener unas opciones incorrectas" do
               expect(@p1.distractor) == ["a) Una instancia de la clase Class \n ", "b) Una constante \n", "d) Ninguna de las anteriores \n"]	       
	    end	
	    it "Debe tener un metodo to_s" do
               expect(@p1).to respond_to :to_s       
	    end	
	    
	  end	
  
	     @p3=Question::SeleccionSimple.new(
					:text => 'class Array \n 
					def say_hi \n
					HEY! 
					end
					end \n
					p[1,, bob].say_hi',:right => 'Ninguna de las anteriores',
 				 	:distractor => ['1', 'bob', 'HEY!'])	      

	      @node3 = Node.new(@p3,nil)
	      
	      
	      
	      
	      
	      @p5=Question::SeleccionSimple.new(
					:text => 'salida de :
 				        class Objeto \n
					end',:right => 'Una instancia de la clase Class',
 				 	:distractor => ['una constante', 'un objeto', 'ninguna de las anteriores'])

	      @node5 = Node.new(@p5,nil)
    
    end#fin del describe de Seleccion Simple
    
 

    describe QuestionExamen::VerdaderoFalso do	  
	  before :each do
	      @p2=QuestionExamen::VerdaderoFalso.new(
					"Es apropiado que una clase Tablero herede de una clase Juego \n", "Verdadero \n",
 				 	"Falso \n")
	      @node2 = Node.new(@p2,nil)	      
	  end	  
	  
	  context "Pruebas para la clase de Verdadero y Falso" do
	     it "Es de la clase SeleccionSimple" do
               expect(@p2.class) ==  VerdaderoFalso      
	    end
	    it "Debe tener un enunciado heredado de la clase Pregunta" do
               expect(@p2.question) == 'Es apropiado que una clase Tablero herede de una clase Juego \n'	       
	    end
	    it "Debe tener una respuesta correcta" do
               expect(@p2.right) == 'Verdadero \n'	       
	    end	
            it "Debe tener unas opciones incorrectas" do
               expect(@p2.wrong) == 'Falso \n'	       
	    end	
	    it "Debe tener un metodo to_s" do
               expect(@p2).to respond_to :to_s       
	    end		    
	  end	    
    end#fin del describe de Verdadero falso
    
     describe QuestionExamen::List do	  
	  before :each do
	      @lista=QuestionExamen::List.new()
	      @lista.push(@node1)
	      @lista.push(@node2)
 	      @lista.push(@node3)
	      @lista.push(@node5)
	      	      
	  end	  
	  
	  context "Pruebas para Node y Lista" do
	        it "Es de la clase List" do
                 expect(@lista.class) ==  List       
	        end
	       it "Debe existir un Nodo de la lista con sus datos, su siguiente y su anterior" do
	         expect(@lista.cabeza != nil && @lista.next == nil && @lista.anterior == nil) 
	       end
	       
	        it "Se deben insertar nodos en la lista" do
                    @lista.push(@node1)
                    expect(@lista.cabeza).to eq(@node1)
                end
                
                it "Se insertan varios elementos por el principio" do
                    @lista.push(@node1)
                    @lista.push(@node2)
                    expect(@lista.cabeza).to eq(@node2)
                end
		
		it "Se insertan varios elementos por el final" do
                    @lista.push_final(@node1)
                    @lista.push_final(@node2)
                    expect(@lista.cabeza).to eq(@node1)
                end

	        it "Se extrae el primer elemento de la lista" do
		    @lista.push(@node1)
                    @lista.push(@node2)
                    @lista.pop
                    expect(@lista.cabeza).to eq(@node1)	
	        end
		
		it "Se extrae el ultimo elemento de la lista" do
		    @lista.push_final(@node1)
                    @lista.push_final(@node2)
                    @lista.pop_final
                    expect(@lista.cabeza).to eq(@node1)	
	        end
		
		it "Se inserta por el final de la lista" do
		    @lista.push_final(@node1)
                    @lista.push_final(@node2)
                    expect(@lista.cabeza).to eq(@node1)	
	        end
	  end
    end#fin del describe de List
    


end
